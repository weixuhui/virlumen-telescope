# virlumen-telescope

###### 继承自laravel官方的 laravel/telescope, 为lumen做了简化处理 
###### (由于个人能力有效, 仅做了一些常用事件的处理)

### 和 laravel/telescope 的区别
- 由于lumen 相对于 laravel 简化了很多, 所以有一些laravel的事件, 无法再直接监听, 其中最主要的就是没有了对外提供的 app()->terminating() 方法, 而 laravel/telescope 的数据入库确是在这里处理的. 所以不得已只能将这里的处理, 分发到不同的地方处理
- 对于request请求, 由于 lumen 没有 RequestHandled 的事件监听, 所以放在了中间件来生成数据, 并使用 Terminable 中间件来保存数据
- 对于 command 命令, 修改了 artisan 脚本, 同样使用 terminate 的方法, 写入数据

### 使用方法

1. `composer require virchow/virlumen-telescope`
2. 在 `bootstrap/app.php` 中, 引入中间件和注册服务类
``` 
    ...
    $app->middleware([
        Virchow\VirlumenTelescope\Middleware\Telescope::class
    ]);
    ...
    $app->register(Virchow\VirlumenTelescope\Providers\TelescopeProdiver::class);
    ...
```
    ps:: 由于默认监听了 redis, 所以在使用前, 建议做好redis的注册, 可以直接使用 `illuminate/redis`
3. 使用lumen的config创建方法, 创建 telescope.config 并注册
4. 迁移数据库文件
5. 如果需要监听 command 命令, 需要修改 `artisan` 脚本, 手动调用 terminate 方法, 如下
```
    $kernel = $app->make(
        'Illuminate\Contracts\Console\Kernel'
    );
    
    // exit($kernel->handle(new ArgvInput, new ConsoleOutput));
    
    $status = $kernel->handle($input = new ArgvInput, new ConsoleOutput);
    $kernel->terminate($input, $status);
    exit($status);
```

### 说明
- 由于我更多的是使用做api服务, 所以将 laravel/telescope 的视图等服务全部移除了. 为了能够查询到数据, 我又使用laravel框架搭建了一个 telescope 的监测系统, 能够添加多个项目的监测. 详见 https://gitee.com/weixuhui/telescope-dashboard.git
