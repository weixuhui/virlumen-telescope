<?php

namespace Virchow\VirlumenTelescope\Storage;

use Illuminate\Database\Eloquent\Model;

class EntryModel extends Model
{
    protected $table = 'telescope_entries';

    const UPDATED_AT = null;

    protected $casts = [
        'content' => 'json',
    ];

    protected $primaryKey = 'uuid';

    protected $keyType = 'string';

    public $incrementing = false;
}
