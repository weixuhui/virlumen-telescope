<?php

namespace Virchow\VirlumenTelescope\Middleware;

use Closure;
use Illuminate\Support\Facades\Log;
use Virchow\VirlumenTelescope\Contracts\EntriesRepository;
use Virchow\VirlumenTelescope\IncomingEntry;
use Virchow\VirlumenTelescope\Storage\DatabaseEntriesRepository;
use Virchow\VirlumenTelescope\Watchers\QueryWatcher;
use Virchow\VirlumenTelescope\Watchers\RequestWatcher;

class Telescope
{
    public function handle($request, Closure $next){
        $response = $next($request);
        app(RequestWatcher::class)->recordRequest($request, $response);
        return $response;
    }

    public function terminate(){
        app('telescope')->store(app('databaseEntriesRepository'));
    }
}
