<?php

namespace Virchow\VirlumenTelescope\Providers;

use Illuminate\Support\Facades\Route;
use Illuminate\Support\ServiceProvider;
use Virchow\VirlumenTelescope\IncomingEntry;
use Virchow\VirlumenTelescope\Storage\DatabaseEntriesRepository;
use Virchow\VirlumenTelescope\Telescope;
use Virchow\VirlumenTelescope\Console;

class TelescopeProdiver extends ServiceProvider
{
    /**
     * Register any package services.
     *
     * @return void
     */
    public function register()
    {
        if (! config('telescope.enabled')) {
            return;
        }
        $this->mergeConfigFrom(
            __DIR__.'/../../config/telescope.php', 'telescope'
        );
        $this->app->singleton('telescope', Telescope::class);
        $this->registerStorageDriver();
        $this->hideSensitiveRequestDetails();
        // Telescope::filter(function (IncomingEntry $entry) {
        //     if ($this->app->environment('local')) {
        //         return true;
        //     }
        //     return $entry->isReportableException() ||
        //         $entry->isFailedRequest() ||
        //         $entry->isFailedJob() ||
        //         $entry->isScheduledTask() ||
        //         $entry->hasMonitoredTag();
        // });
    }

    /**
     * Bootstrap any package services.
     *
     * @return void
     */
    public function boot()
    {
        if (! config('telescope.enabled')) {
            return;
        }
        $this->registerCommands();
        Telescope::start($this->app);
    }

    /**
     * Register the package's commands.
     *
     * @return void
     */
    protected function registerCommands()
    {
        if ($this->app->runningInConsole()) {
            $this->commands([
                Console\ClearCommand::class,
                Console\PruneCommand::class,
            ]);
        }
    }

    /**
     * Register the package storage driver.
     *
     * @return void
     */
    protected function registerStorageDriver()
    {
        $driver = config('telescope.driver');
        if (method_exists($this, $method = 'register'.ucfirst($driver).'Driver')) {
            $this->$method();
        }
    }

    /**
     * Register the package database storage driver.
     *
     * @return void
     */
    protected function registerDatabaseDriver()
    {
        $this->app->singleton('databaseEntriesRepository', DatabaseEntriesRepository::class);

        $this->app->when(DatabaseEntriesRepository::class)
            ->needs('$connection')
            ->give(config('telescope.storage.database.connection'));

        $this->app->when(DatabaseEntriesRepository::class)
            ->needs('$chunkSize')
            ->give(config('telescope.storage.database.chunk'));
    }

    /**
     * Prevent sensitive request details from being logged by Telescope.
     *
     * @return void
     */
    protected function hideSensitiveRequestDetails()
    {
        if ($this->app->environment('local')) {
            return;
        }
        Telescope::hideRequestParameters(['_token']);
        Telescope::hideRequestHeaders([
            'cookie',
            'x-csrf-token',
            'x-xsrf-token',
        ]);
    }
}
